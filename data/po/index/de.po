# 
# Translators:
# FortressBuilder <fellner.sebastian@gmail.com>, 2016
# FortressBuilder <fellner.sebastian@gmail.com>, 2016
# Stephen, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-06 16:10+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: German (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: index.html:14
msgid "Main Menu"
msgstr "Hauptmenü"

#: index.html:17
msgid ""
"Ensures Welcome's documentation, translations and software picks are always "
"up-to-date."
msgstr ""

#: index.html:18
msgid "Subscribe to Welcome updates"
msgstr "Willkommen-Aktualisierungen abonnieren"

#: index.html:22
msgid "Welcome will automatically restart in a few moments..."
msgstr "Willkommen wird in wenigen Augenblicken automatisch neu gestartet..."

#: index.html:37
msgid "Choose an option to discover your new operating system."
msgstr "Wählen Sie eine Option aus um Ihr neues Betriebssystem zu entdecken."

#: index.html:45
msgid "Introduction"
msgstr "Einführung"

#: index.html:46
msgid "Features"
msgstr "Eigenschaften"

#: index.html:47
msgid "Getting Started"
msgstr "Erste Schritte"

#: index.html:48
msgid "Installation Help"
msgstr "Installationshilfe"

#: index.html:53
msgid "Get Involved"
msgstr "Mitmachen"

#: index.html:54
msgid "Shop"
msgstr "Einkaufen"

#: index.html:55
msgid "Donate"
msgstr "Spenden"

#: index.html:63
msgid "Community"
msgstr "Gemeinde"

#: index.html:64
msgid "Chat Room"
msgstr "Chatraum"

#: index.html:65
msgid "Software"
msgstr "Software"

#: index.html:66
msgid "Install Now"
msgstr "Jetzt installieren"

#: index.html:73
msgid "Raspberry Pi Information"
msgstr "Raspberry-Pi-Informationen"

#: index.html:86
msgid "Open Welcome when I log on."
msgstr "Willkommen nach der Anmeldung öffnen."
