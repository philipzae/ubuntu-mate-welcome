# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-06 16:09+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Bulgarian (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/bg/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bg\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: index.html:14
msgid "Main Menu"
msgstr ""

#: index.html:17
msgid ""
"Ensures Welcome's documentation, translations and software picks are always "
"up-to-date."
msgstr ""

#: index.html:18
msgid "Subscribe to Welcome updates"
msgstr ""

#: index.html:22
msgid "Welcome will automatically restart in a few moments..."
msgstr ""

#: index.html:37
msgid "Choose an option to discover your new operating system."
msgstr ""

#: index.html:45
msgid "Introduction"
msgstr ""

#: index.html:46
msgid "Features"
msgstr ""

#: index.html:47
msgid "Getting Started"
msgstr ""

#: index.html:48
msgid "Installation Help"
msgstr ""

#: index.html:53
msgid "Get Involved"
msgstr ""

#: index.html:54
msgid "Shop"
msgstr ""

#: index.html:55
msgid "Donate"
msgstr ""

#: index.html:63
msgid "Community"
msgstr ""

#: index.html:64
msgid "Chat Room"
msgstr ""

#: index.html:65
msgid "Software"
msgstr ""

#: index.html:66
msgid "Install Now"
msgstr ""

#: index.html:73
msgid "Raspberry Pi Information"
msgstr ""

#: index.html:86
msgid "Open Welcome when I log on."
msgstr ""
