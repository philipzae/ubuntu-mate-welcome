# 
# Translators:
# Guz Firdaus <guzfirdaus@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-05-06 09:24+0000\n"
"Last-Translator: Guz Firdaus <guzfirdaus@gmail.com>\n"
"Language-Team: Indonesian (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/id/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: id\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: donate.html:17
msgid "Donate"
msgstr "Donasi"

#: donate.html:25
msgid "About Donations"
msgstr "Tentang Donasi"

#: donate.html:26
msgid "Our Supporters"
msgstr "Pendukung Kami"

#: donate.html:29
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "Sepertinya kamu tidak terhubung dengan internet. Silahkan periksa koneksi kamu untuk mengakses konten ini."

#: donate.html:30
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "Maaf, Selamat Datang tidak dapat membuat sambungan."

#: donate.html:31
msgid "Retry"
msgstr "Coba lagi"

#: donate.html:40
msgid ""
"Ubuntu MATE is funded by our community. Your donations help pay towards:"
msgstr ""

#: donate.html:42
msgid "Web hosting and bandwidth costs."
msgstr "Web hosting dan biaya bandwidth."

#: donate.html:43
msgid "Supporting Open Source projects that Ubuntu MATE depends upon."
msgstr ""

#: donate.html:44
msgid ""
"Eventually pay for full time developers of Ubuntu MATE and MATE Desktop."
msgstr ""

#: donate.html:46
msgid "Patreon"
msgstr "Patreon"

#: donate.html:46
msgid ""
"is a unique way to fund an Open Source project. A regular monthly income "
"ensures sustainability for the Ubuntu MATE project. Patrons will be rewarded"
" with exclusive project news, updates and invited to participate in video "
"conferences where you can talk to the developers directly."
msgstr ""

#: donate.html:51
msgid "If you would prefer to use"
msgstr ""

#: donate.html:51
msgid ""
"then we have options to donate monthly or make a one off donation. Finally, "
"we also accept donations via"
msgstr ""

#: donate.html:53
msgid "Bitcoin"
msgstr "Bitcoin"

#: donate.html58, 71
msgid "Become a Patron"
msgstr "Menjadi Patreon"

#: donate.html:61
msgid "Donate with PayPal"
msgstr "Donasi dengan PayPal"

#: donate.html:64
msgid "Donate with Bitcoin"
msgstr "Donasi dengan Bitcoin"

#: donate.html:70
msgid "Commercial sponsorship"
msgstr "Sponsor komersil"

#: donate.html:70
msgid "is also available. Click the"
msgstr "juga tersedia. Klik"

#: donate.html:71
msgid "button above to find out more."
msgstr "tombol di atas untuk keterangan lebih lanjut."

#: donate.html:79
msgid "Thank you!"
msgstr "Terima kasih!"

#: donate.html:80
msgid ""
"Every month, we compile a list of our donations and detail how they are "
"spent."
msgstr ""

#: donate.html:81
msgid "This information is published monthly to the"
msgstr "Informasi ini diumumkan setiap bulan kepada"

#: donate.html:81
msgid "Ubuntu MATE Blog"
msgstr "Blog Ubuntu MATE"

#: donate.html:82
msgid "which you can take a look below."
msgstr "yang dapat Anda lihat di bawah ini."

#: donate.html:88
msgid "Year"
msgstr "Tahun"

#: donate.html:89
msgid "Month"
msgstr "Bulan"
